/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';

//

const App = () => {
  const [count, setCount] = useState(0);

  function onButtonPress() {
    console.log('heeej');

    setCount(count + 1);
  }

  function onButtonPressIn() {
    console.log('IN');
  }
  function onButtonPressOut() {
    console.log('OUT');
  }

  return (
    <View>
      <Text style={{marginTop: 100}}>{'hej'}</Text>

      <Button title={'Kliknij mnie'} />

      <TouchableOpacity
        activeOpacity={0.3}
        onPress={onButtonPress}
        onPressIn={onButtonPressIn}
        onPressOut={onButtonPressOut}>
        <Text>{'KLIK'}</Text>
      </TouchableOpacity>

      <TouchableHighlight
        activeOpacity={0.3}
        style={{backgroundColor: 'black'}}
        underlayColor={'blue'}
        onPress={() => {}}>
        <Text>{'Inny klik'}</Text>
      </TouchableHighlight>

      <View>
        <Text>{`Kliked: ${count}`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});

export default App;
